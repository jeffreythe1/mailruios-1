//
//  Created by Dmitry Sochnev.
//  Copyright © 2020 Applicatura. All rights reserved.
//

import UIKit

protocol AppControllerProtocol {
    func setup()
}

final class AppController {

    weak var window: UIWindow?

    init(window: UIWindow?) {
        self.window = window
    }
}

extension AppController: AppControllerProtocol {
    func setup() {
        setupAppearance()
        setupInitialViewController()
    }
}

// MARK: Private
extension AppController {
    func setupAppearance() {
        let appearance = UINavigationBar.appearance()
        appearance.tintColor = .appGray4
        appearance.titleTextAttributes = [
            .font: UIFont.bold17,
            .foregroundColor: UIColor.appForeground
        ]
    }

    // MARK: - Initial screen
    private func setupInitialViewController() {
        let vc = ScenesFactory.shared.contactsList()
        let navVC = UINavigationController(rootViewController: vc)
        window?.rootViewController = navVC
        window?.makeKeyAndVisible()
    }
}
