//
//  Created by Dmitry Sochnev.
//  Copyright © 2020 Applicatura. All rights reserved.
//

import UIKit

final class AvatarView: UIView {

    enum Style {
        case thumbnail
        case large
    }

    private let borderColor: UIColor = .appGray4

    private let initialsLabel = UILabel(style: .title1, lines: 1, alignment: .center)

    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()

    private var style: Style = .thumbnail
    private var width: CGFloat = 0.0
    private var borderWidth: CGFloat = 0.0

    convenience init(style: Style) {
        self.init()
        self.style = style
        setup()
    }

    func cleanup() {
        imageView.image = nil
    }

    func setAvatar(_ avatar: Contact.Avatar) {
        switch avatar {
        case .image(let image):
            initialsLabel.isHidden = true
            imageView.isHidden = false
            imageView.image = image
            layer.borderWidth = 0

        case .initials(let initials):
            initialsLabel.isHidden = false
            imageView.isHidden = true
            initialsLabel.text = initials
            layer.borderWidth = borderWidth
        }
    }

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)

        if #available(iOS 13.0, *) {
            if traitCollection.hasDifferentColorAppearance(comparedTo: previousTraitCollection) {
                layer.borderColor = borderColor.resolvedColor(with: traitCollection).cgColor
            }
        }
    }
}

// MARK: - Private -

private extension AvatarView {

    func setup() {
        switch style {
        case .thumbnail:
            width = 60
            borderWidth = width / 30
        case .large:
            width = 180
            borderWidth = width / 30
        }

        layer.cornerRadius = width / 2
        layer.borderWidth = borderWidth
        layer.borderColor = borderColor.cgColor
        clipsToBounds = true

        backgroundColor = .appGray1

        initialsLabel.textColor = .appGray4
        initialsLabel.font = initialsLabel.font.withSize(floor(width / 2.15))

        addSubview(initialsLabel)
        initialsLabel.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }

        addSubview(imageView)
        imageView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }

        snp.makeConstraints {
            $0.width.height.equalTo(width)
        }
    }
}
