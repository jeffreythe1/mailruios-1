//
//  Created by Dmitry Sochnev.
//  Copyright © 2020 Applicatura. All rights reserved.
//

import UIKit

extension UIColor {
    static let appBlue = color(light: .lightBlue, dark: .darkBlue)
    static let appGray1 = color(light: .lightGray1, dark: .darkGray1)
    static let appGray2 = color(light: .lightGray2, dark: .darkGray2)
    static let appGray3 = color(light: .lightGray3, dark: .darkGray3)
    static let appGray4 = color(light: .lightGray4, dark: .darkGray4)
    static let appForeground = color(light: .black, dark: .white)
    static let appBackground = color(light: .white, dark: .black)
    static let appWhite = white
    static let appBlack = black
}

extension UIColor {
    static func color(light: UIColor, dark: UIColor) -> UIColor {
        if #available(iOS 13, *) {
            return UIColor { traitCollection -> UIColor in
                traitCollection.userInterfaceStyle == .dark ? dark : light
            }
        } else {
            return light
        }
    }
}
