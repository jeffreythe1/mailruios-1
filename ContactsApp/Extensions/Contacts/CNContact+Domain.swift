//
//  Created by Dmitry Sochnev.
//  Copyright © 2020 Applicatura. All rights reserved.
//

import Contacts

extension CNContact {
    var domainModel: Contact {
        return Contact(
            imageData: imageData,
            thumbnailImageData: thumbnailImageData,
            givenName: givenName,
            middleName: middleName,
            familyName: familyName,
            organizationName: organizationName,
            phones: phoneNumbers.phones
        )
    }
}

extension Array where Element == CNContact {
    var domainModels: [Contact] {
        map { $0.domainModel }
    }
}

extension Array where Element == CNLabeledValue<CNPhoneNumber> {
    var phones: [Phone] {
        compactMap { phoneNumber in
            Phone(label: Element.localizedString(forLabel: phoneNumber.label ?? L.phone.title.lowercased()),
                  phoneNumber: phoneNumber.value.stringValue,
                  isMain: phoneNumber.label == CNLabelPhoneNumberMain)
        }
    }
}
