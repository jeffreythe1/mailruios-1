//
//  Created by Dmitry Sochnev.
//  Copyright © 2020 Applicatura. All rights reserved.
//

import UIKit

typealias Attributes = [NSAttributedString.Key: Any]

public enum AnyString {
    case string(String)
    case attributed(NSAttributedString)
}

extension UILabel {

    func set(text: AnyString?) {
        guard let text = text else {
            self.attributedText = nil
            self.text = nil
            return
        }

        switch text {
        case .string(let str):
            self.attributedText = nil
            self.text = str
        case .attributed(let str):
            self.text = nil
            self.attributedText = str
        }
    }

    convenience init(style: Style, text: String? = nil , lines: Int = 0, alignment: NSTextAlignment = .natural) {
        self.init()
        self.font = style.font
        self.textColor = style.color
        self.text = text
        self.textAlignment = alignment
        self.numberOfLines = lines
    }
}

extension UILabel {
    enum Style: String {
        case title1
        case title2
        case title3
        case body
        case description

        var attributes: Attributes {
            Self.attributesInfo[self] ?? [:]
        }

        var font: UIFont {
            attributes[.font] as! UIFont
        }

        var color: UIColor {
            attributes[.foregroundColor] as! UIColor
        }

        private static let attributesInfo: [Self: Attributes] = [
            .title1: [
                .font: UIFont.bold28,
                .foregroundColor: UIColor.appForeground
            ],
            .title2: [
                .font: UIFont.bold22,
                .foregroundColor: UIColor.appForeground
            ],
            .title3: [
                .font: UIFont.bold20,
                .foregroundColor: UIColor.appForeground
            ],
            .body: [
                .font: UIFont.regular17,
                .foregroundColor: UIColor.appForeground
            ],
            .description: [
                .font: UIFont.regular16,
                .foregroundColor: UIColor.appGray3
            ]
        ]
    }
}
