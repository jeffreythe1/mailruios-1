//
//  Created by Dmitry Sochnev.
//  Copyright © 2020 Applicatura. All rights reserved.
//

import RxSwift

extension ObservableType {
    func subscribeNext<T: AnyObject>(_ instance: T,
                                     with classFunc: @escaping (T) -> (Self.Element) -> Void) -> Disposable {
        subscribe(onNext: { [weak instance] args in
            guard let instance = instance else { return }
            let instanceFunction = classFunc(instance)
            instanceFunction(args)
        })
    }

    func subscribeNext<T: AnyObject>(_ instance: T,
                                     do classFunc: @escaping (T) -> () -> Void) -> Disposable {
        subscribe(onNext: { [weak instance] _ in
            guard let instance = instance else { return }
            let instanceFunction = classFunc(instance)
            instanceFunction()
        })
    }

    func subscribeNext<T: AnyObject>(_ instance: T,
                                     with classFunc: @escaping (T) -> (Self.Element) -> Void,
                                     bag: DisposeBag) {
        subscribe(onNext: { [weak instance] args in
            guard let instance = instance else { return }
            let instanceFunction = classFunc(instance)
            instanceFunction(args)
        }).disposed(by: bag)
    }

    func subscribeNext<T: AnyObject>(_ instance: T,
                                     do classFunc: @escaping (T) -> () -> Void,
                                     bag: DisposeBag) {
        subscribe(onNext: { [weak instance] _ in
            guard let instance = instance else { return }
            let instanceFunction = classFunc(instance)
            instanceFunction()
        }).disposed(by: bag)
    }
}

