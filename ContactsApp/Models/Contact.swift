//
//  Created by Dmitry Sochnev.
//  Copyright © 2020 Applicatura. All rights reserved.
//

import UIKit

struct Contact {
    let imageData: Data?
    let thumbnailImageData: Data?
    let givenName: String
    let middleName: String
    let familyName: String
    let organizationName: String
    let phones: [Phone]
}

extension Contact: Equatable {}

extension Contact {
    enum AccentName {
        case given
        case middle
        case family
        case organization
        case none
    }
    var accentName: AccentName {
        if !familyName.isEmpty {
            return .family
        } else if !givenName.isEmpty {
            return .given
        } else if !middleName.isEmpty {
            return .middle
        } else if !organizationName.isEmpty {
            return .organization
        }
        return .none
    }

    var sectionTitle: String {
        let names = [familyName, givenName, middleName, organizationName]
        let title = names.compactMap { $0.first?.uppercased() }.first
        if let title = title, Int(title) == nil {
            return title
        } else {
            return "#"
        }
    }

    var fullName: String {
        var fullName = [givenName, middleName, familyName]
            .compactMap { $0.isEmpty ? nil : $0 }
            .joined(separator: " ")
        if fullName.isEmpty {
            fullName = organizationName
        }
        return fullName
    }

    var avatar: Avatar {
        if let imageData = imageData, let image = UIImage(data: imageData) {
            return .image(image)
        } else {
            let initials = [givenName, familyName].compactMap { $0.first?.uppercased() }.joined()
            if initials.isEmpty {
                let letters = organizationName
                    .split(separator: " ")
                    .compactMap { $0.first?.uppercased() }
                    .joined()
                    .prefix(2)
                return .initials(String(letters))
            } else {
                return .initials(initials)
            }
        }
    }
}

extension Contact {
    enum Avatar {
        case image(UIImage)
        case initials(String)

        var image: UIImage? {
            guard case let .image(image) = self else { return nil }
            return image
        }

        var initials: String? {
            guard case let .initials(initials) = self else { return nil }
            return initials
        }
    }
}

extension Array where Element == Phone {
    var mainOrNext: Phone? {
        mainFirst.first
    }

    var mainFirst: [Phone] {
        sorted { $0.isMain && !$1.isMain }
    }
}
