//
//  Created by Dmitry Sochnev.
//  Copyright © 2020 Applicatura. All rights reserved..
//

import UIKit
import RxSwift
import RxCocoa

final class ContactDetailVC: UIViewController {

    typealias ViewModel = ContactDetailVM

    // MARK: - Properties

    var viewModel: ContactDetailVM!

    private let disposeBag = DisposeBag()

    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.addSubview(stackView)
        stackView.snp.makeConstraints {
            $0.edges.equalToSuperview().inset(16)
        }
        return scrollView
    }()

    private lazy var stackView: UIStackView = {
        UIStackView(.vertical, spacing: 32, views: [
            UIStackView(.vertical, spacing: 32, alignment: .center, views: [
                avatarView,
                fullNameLabel
            ]),
            phoneItemsStack
        ])
    }()

    private let avatarView = AvatarView(style: .large)

    private let fullNameLabel = UILabel(style: .title1, alignment: .center)

    private let phoneItemsStack = UIStackView(.vertical, spacing: 16, views: [])

    // MARK: - Override

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        bind()
    }
}

// MARK: - Bindings -

private extension ContactDetailVC {
    func bind() {
        viewModel.contact
            .observeOn(MainScheduler.instance)
            .subscribeNext(self, with: ContactDetailVC.setModel, bag: disposeBag)
    }
}

// MARK: - Setup UI -

private extension ContactDetailVC {
    func setupUI() {
        if #available(iOS 11.0, *) {
            navigationItem.largeTitleDisplayMode = .never
        }

        view.backgroundColor = .appBackground

        view.addSubview(scrollView)
        scrollView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }

        stackView.snp.makeConstraints {
            $0.width.equalTo(view.snp.width).inset(16)
        }
    }

    func setModel(_ model: Contact) {
        // Thumbnail
        avatarView.setAvatar(model.avatar)

        // Full name
        fullNameLabel.set(text: .string(model.fullName))

        // Phones
        let phoneViews: [UIView] = model.phones.mainFirst.map { phone in
            let view = UIView()
            view.layer.cornerRadius = 8
            view.backgroundColor = .appGray1
            let stack = UIStackView(.vertical, spacing: 4, views: [
                UILabel(style: .body, text: phone.label, lines: 1),
                UILabel(style: .title3, text: phone.phoneNumber, lines: 1)
            ])
            view.addSubview(stack)
            stack.snp.makeConstraints {
                $0.edges.equalToSuperview().inset(16)
            }
            
            view.addTapGestureRecognizer { [weak self] in
                self?.viewModel.fire(action: .callTo(phone.phoneNumber))
            }
            return view
        }
        phoneItemsStack.removeAllArrangedSubviews()
        phoneItemsStack.add(arrangedSubviews: phoneViews)
    }
}
