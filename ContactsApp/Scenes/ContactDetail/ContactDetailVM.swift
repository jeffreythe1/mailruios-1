//
//  Created by Dmitry Sochnev.
//  Copyright © 2020 Applicatura. All rights reserved..
//

import RxSwift
import RxRelay

final class ContactDetailVM: VM {

    enum Action {
        case callTo(String)
    }

    // MARK: - Properties

    var router: ContactDetailRouter!

    private let _contact: BehaviorRelay<Contact>

    // MARK: - Public

    lazy var contact = _contact.asObservable()

    init(contact: Contact) {
        _contact = BehaviorRelay<Contact>(value: contact)
    }

    func reduce(_ action: Action) {
        switch action {
        case .callTo(let phoneNumber):
            router.callTo(phoneNumber)
        }
    }
}

// MARK: - Private -

private extension ContactDetailVM {

}
