//
//  Created by Dmitry Sochnev.
//  Copyright © 2020 Applicatura. All rights reserved..
//

import Foundation

final class ContactsListRouter: Router {
    func openContactDetail(_ contact: Contact) {
        let controller = ScenesFactory.shared.contactDetail(contact)
        present(controller)
    }
}
