//
//  Created by Dmitry Sochnev.
//  Copyright © 2020 Applicatura. All rights reserved..
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources
import SnapKit

struct ContactsList {
    typealias Section = String
    typealias Item = Contact
}

extension Contact: IdentifiableType {
    public var identity: String {
        "\(fullName)"
    }
}

typealias ContactsListSection = AnimatableSectionModel<ContactsList.Section, ContactsList.Item>

final class ContactsListVC: UIViewController {

    typealias ViewModel = ContactsListVM

    // MARK: - Properties

    var viewModel: ViewModel!

    private let disposeBag = DisposeBag()

    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.separatorStyle = .none
        tableView.tableFooterView = UIView()

        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 92

        tableView.register(ContactsListItemCell.self)

        return tableView
    }()

    private lazy var dataSource: RxTableViewSectionedAnimatedDataSource<ContactsListSection> = {
        RxTableViewSectionedAnimatedDataSource<ContactsListSection>(
            configureCell: { [weak self] _, tableView, indexPath, model in
                guard let self = self else { return UITableViewCell() }
                let cell = tableView.dequeueCell(ContactsListItemCell.self, for: indexPath)
                cell.setModel(model)
                return cell
            },
            titleForHeaderInSection: { dataSource, index in
                dataSource.sectionModels[index].model
            },
            sectionIndexTitles: { [weak self] dataSource in
                dataSource.sectionModels.map { $0.model }
            }
        )
    }()

    // MARK: - Override

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        bind()

        viewModel.fire(action: .viewLoaded)
    }
}

// MARK: - Bindings -

private extension ContactsListVC {
    func bind() {
        disposeBag.insert([
            viewModel.sections
                .asDriver(onErrorJustReturn: [])
                .drive(tableView.rx.items(dataSource: dataSource)),

            tableView.rx.modelSelected(Contact.self)
                .map { ViewModel.Action.openContactDetail($0) }
                .subscribeNext(viewModel, with: ViewModel.fire),

            tableView.rx.itemSelected
                .bind { [weak self] in self?.tableView.deselectRow(at: $0, animated: true) }
        ])
    }
}

// MARK: - Setup UI -

private extension ContactsListVC {
    func setupUI() {

        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
        }

        title = L.contacts.title

        view.addSubview(tableView)
        tableView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
}
