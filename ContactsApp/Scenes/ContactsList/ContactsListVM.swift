//
//  Created by Dmitry Sochnev.
//  Copyright © 2020 Applicatura. All rights reserved..
//

import RxSwift
import RxCocoa
import RxRelay

final class ContactsListVM: VM {

    enum Action {
        case viewLoaded
        case openContactDetail(Contact)
    }

    // MARK: - Properties

    var router: ContactsListRouter!

    private let contactService = ContactsService()

    let sections = BehaviorRelay<[ContactsListSection]>(value: [])

    // MARK: - Public

    func reduce(_ action: Action) {
        switch action {
        case .viewLoaded:
            requestAccess { [weak self] success in
                if success {
                    self?.fetchContacts()
                }
            }
        case .openContactDetail(let contact):
            router.openContactDetail(contact)
        }
    }
}

// MARK: - Private -

private extension ContactsListVM {
    func requestAccess(completion: @escaping (Bool) -> Void) {
        contactService.requestAccess(.contacts) { [weak self] result in
            switch result {
            case .success(let granted):
                completion(granted)
            case .failure(let error):
                self?.router.showAlert(title: "Ошибка", message: error.localizedDescription, actions: [.close])
            }
        }
    }

    func fetchContacts() {
        contactService.contacts { [weak self] result in
            switch result {
            case .success(let contacts):
                self?.prepareContacts(contacts)
            case .failure(let error):
                self?.router.showAlert(title: "Ошибка", message: error.localizedDescription, actions: [.close])
            }
        }
    }

    func prepareContacts(_ contacts: [Contact]) {
        let grouped = Dictionary(grouping: contacts, by: { $0.sectionTitle })
        let sharp = CharacterSet(arrayLiteral: "#")
        let sections = grouped
            .map { ContactsListSection(model: $0.key, items: $0.value) }
            .sorted { $0.model.localizedStandardCompare($01.model) == .orderedAscending }
            .sorted {
                if let left = $0.model.unicodeScalars.first, let right = $1.model.unicodeScalars.first {
                    return !sharp.contains(left) && sharp.contains(right)
                }
                return false
            }

        self.sections.accept(sections)
    }
}
