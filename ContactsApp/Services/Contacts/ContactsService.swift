//
//  Created by Dmitry Sochnev.
//  Copyright © 2020 Applicatura. All rights reserved.
//

import Contacts

final class ContactsService {
    private let store = CNContactStore()

    func requestAccess(_ entityType: CNEntityType, completion: @escaping (Result<Bool, Error>) -> Void) {
        store.requestAccess(for: entityType, completionHandler: { granted, error in
            if let error = error {
                completion(.failure(error))
            } else {
                completion(.success(granted))
            }
        })
    }

    func contacts(completion: @escaping (Result<[Contact], Error>) -> Void) {
        let keys = [
            CNContactImageDataKey,
            CNContactThumbnailImageDataKey,
            CNContactGivenNameKey,
            CNContactMiddleNameKey,
            CNContactFamilyNameKey,
            CNContactOrganizationNameKey,
            CNContactPhoneNumbersKey
        ] as [CNKeyDescriptor]
        let request = CNContactFetchRequest(keysToFetch: keys)
        do {
            var contacts: [CNContact] = []
            try store.enumerateContacts(with: request) { contact, _ in
                contacts.append(contact)
            }
            completion(.success(contacts.domainModels))
        } catch {
            completion(.failure(error))
        }
    }
}
